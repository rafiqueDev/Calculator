package calculate;

/**
 * Created by abdulrafique on 10/11/2016.
 */
public class Subtraction implements Calculate {
    @Override
    public int calculate(int a, int b) {
        return a-b;
    }
}
