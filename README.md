[![Build status](https://gitlab.com/NBNAUSTRALIA/Calculator/badges/master/build.svg)](https://gitlab.com/NBNAUSTRALIA/Calculator/commits/master)
[![License](http://img.shields.io/:license-apache-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0.html)

# Project: Calculator
Calculation for addition.

## Language
Java


## Upgrading

For upgrading information please see our [update page](https://about.gitlab.com/update/).

## Documentation

All documentation can be found on [docs.gitlab.com/ce/](https://docs.gitlab.com/ce/).

## Getting help

Please see [Getting help for GitLab](https://about.gitlab.com/getting-help/) on our website for the many options to get help.